//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <QDebug>
#include <QElapsedTimer>

// devices-test
#include "testcontroller.h"

TestController::TestController(QObject *parent)
    :
        QObject(parent),
        m_input_seq(0), m_digit(0), m_segment(0),
        m_lcd(
            new RGBLCD(
                0x20,
                RGBLCD_PinMap(
                    // rs, rw, en, b0, b1, b2, b3, b4, b5, b6, b7, r, g, b
                    0,     99,  1,  2,  3,  4,  5,  9, 10, 11, 12, 6, 7, 8
                ),
                4, 2, 16
            )
        ),
        m_led(new MAX7219(10, 12, 14, 8)),
        m_io(new MCP23017(0x21))
{


    m_lcd->set_backlight(false, false, true);
    m_lcd->set_display(1);
    m_lcd->clear_display();
    m_lcd->write_text("Starting...");

    m_led->set_intensity(0xf);
    m_led->set_digit(0, 0);
    m_led->set_digit(1, 0);

    m_io->set_pin_modes(0x0000); delay(150); // all inputs
    m_io->set_pull_off (0xFFFF); delay(150); // reset pull-ups
    m_io->set_pull_up  (0xFFFF); delay(150); // all pull-up
    connect(
        m_io, &MCP23017::interrupt,
        this, &TestController::on_interrupt
    );
    m_io->enable_interrupt(0xFFFF, 0xFFFF, INT_EDGE_RISING, 0);
}


void TestController::on_interrupt(quint16 /*trigger*/, quint16 /*state*/)
{
    QElapsedTimer t;
    t.start();

    QString m;
    QTextStream ms(&m);
    ms << m_digit << ":" << m_segment;
    m_led->set_digit(m_digit, 1 << m_segment);
    m_lcd->write_text(m);

    m_segment = (m_segment + 1) % 8;
    if (m_segment == 0) {
        m_led->set_digit(m_digit, 0);
        m_digit = (m_digit + 1) % 2;
    }

    m_input_seq = (m_input_seq + 1) % 11;

    qDebug() << "test interrupt handler took " << (t.nsecsElapsed() / 1.0e6) << "ms";
}
