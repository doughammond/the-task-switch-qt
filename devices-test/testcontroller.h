//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QTextStream>
#include <QTimer>


// tts-devices
#include <tts-rgblcd.h>
#include <tts-max7219.h>
#include <tts-mcp23017.h>

class TestController : public QObject
{
    Q_OBJECT

public:
    explicit TestController(QObject *parent = nullptr);

signals:

public slots:
    void on_interrupt(quint16 trigger, quint16 state);

private:
    QTimer m_tmr;
    quint8 m_input_seq;
    quint8 m_digit;
    quint8 m_segment;

    RGBLCD *m_lcd;
    MAX7219 *m_led;
    MCP23017 *m_io;
};
