//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <QDebug>
#include <QString>
#include <QtTest>

// redux
#include <store.h>
#include <reducer.h>
#include <reducers/null.h>
#include <middleware/saga.h>


class Test_Store : public QObject
{
    Q_OBJECT

public:
    Test_Store();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void test_basicFunction();
    void test_basicReducer();
    void test_basicMiddleware();
    void test_sagaMiddleware();
};


// -----------------------------------------


Test_Store::Test_Store()
= default;

void Test_Store::initTestCase()
{

}

void Test_Store::cleanupTestCase()
{

}


// -----------------------------------------


void Test_Store::test_basicFunction()
{
    auto *store = new Redux::Store(
        new Redux::NullReducer()
    );

    Redux::State s1 = store->get_state();
    qDebug() << "\nts1 state\n\t\t" << s1 << "\n\n";
    QVERIFY2(s1.isEmpty(), "s1 is not empty");

    Redux::ActionProperties ap2;
    store->dispatch(Redux::makeAction("", ap2));
    Redux::State s2 = store->get_state();
    qDebug() << "\nts2 state\n\t\t" << s2 << "\n\n";
    QVERIFY2(s2.isEmpty(), "s2 is not empty");

    // really testing the behaviour of NullReducer here;
    QVERIFY2(s1 == s2, "s1 is not s2");
}


// -----------------------------------------


class BasicReducer : public Redux::Reducer {
public:
    using SharedPointer = QSharedPointer<BasicReducer>;

    Redux::State reduce(Redux::Action action, Redux::State state) override {
        if (action.first == "TEST_ACTION") {
            Redux::State new_state(state);
            new_state["TEST_PROP"] = action.second;
            return new_state;
        }

        return state;
    }
};

void Test_Store::test_basicReducer() {
    auto *store = new Redux::Store(
        new BasicReducer()
    );

    Redux::State s1 = store->get_state();
    qDebug() << "\nts1 state\n\t\t" << s1 << "\n\n";
    QVERIFY2(s1.isEmpty(), "s1 is not empty");

    Redux::ActionProperties ap2;
    ap2.insert("VALUE KEY", "VALUE VALUE");
    store->dispatch(Redux::makeAction("TEST_ACTION", ap2));
    Redux::State s2 = store->get_state();
    qDebug() << "\nts2 state\n\t\t" << s2 << "\n\n";
    QVERIFY2(!s2.isEmpty(), "s2 is empty");

    // really testing the behaviour of NullReducer here;
    QVERIFY2(s1 != s2, "s1 is s2");
}


// -----------------------------------------


class TestMiddleware : public Redux::Dispatcher {
public:
    TestMiddleware (Redux::Store *store) : Redux::Dispatcher(store) {}

    void dispatch(Redux::Action action) override {
        qDebug() << "\n\tTestMiddleware action\n\t\t" << action << "\n\n";
        // gonna mutate the action ... !
        action.first = "OTHER_ACTION";
        qDebug() << "\n\tTestMiddleware mutated action\n\t\t" << action << "\n\n";
        // chain return
        return m_next->dispatch(action);
    }
};

void Test_Store::test_basicMiddleware() {
    auto *store = new Redux::Store(
        new BasicReducer()
    );

    Redux::MiddlewareList mw;
    mw.push_back(new TestMiddleware(store));
    QVERIFY2(mw[0]->get_next() == nullptr, "TestMiddleware has next dispatcher");

    store->apply_middleware(mw);
    QVERIFY2(mw[0]->get_next() != nullptr, "TestMiddleware has no next dispatcher");

    Redux::ActionProperties ap2;
    ap2.insert("VALUE KEY", "VALUE VALUE");
    store->dispatch(Redux::makeAction("TEST_ACTION", ap2));
    Redux::State s2 = store->get_state();
    qDebug() << "\nts2 state\n\t\t" << s2 << "\n\n";
    // the state is empty because the middleware mutated the action
    QVERIFY2(s2.isEmpty(), "s2 is not empty");
}


// -----------------------------------------


class TestSagaTask : public Redux::SagaTask
{
public:
    Redux::ActionList run(Redux::Action action, Redux::Store * /*store*/) override {
        Redux::ActionList al;
        if (action.first == "SAGA_TRIGGER") {
            // cause a side effect compatible with BasicReducer
            Redux::ActionProperties ap;
            al.push_back(Redux::makeAction("TEST_ACTION", ap));
        }
        return al;
    }
};

void Test_Store::test_sagaMiddleware() {
    auto *store = new Redux::Store(
        new BasicReducer()
    );

    Redux::MiddlewareList mw;
    auto *sagamw = new Redux::SagaMiddleware(store);

    auto *saga = new Redux::Saga();
    auto *task = new TestSagaTask();
    saga->add_action_task("SAGA_TRIGGER", task);
    sagamw->add_saga(saga);

    mw.push_back(sagamw);
    store->apply_middleware(mw);

    Redux::ActionProperties ap1;
    store->dispatch(Redux::makeAction("SAGA_TRIGGER", ap1));
    Redux::State s1 = store->get_state();
    qDebug() << "\nts1 state\n\t\t" << s1 << "\n\n";
    // the state not is empty because the saga dispatched
    // side-effect actions
    QVERIFY2(!s1.isEmpty(), "s1 is empty");
}


// -----------------------------------------

QTEST_APPLESS_MAIN(Test_Store)

#include "tst_store.moc"
