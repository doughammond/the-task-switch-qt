//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// redux
#include "types.h"

namespace Redux {

// Callable (function) which is a dispatcher;
// but for reasons of OO programming, this is
// an object which encapsulates the dispatching
// context and implements a dispatch() method.
class Dispatcher {
public:

    Dispatcher(Store *store, Dispatcher *next = nullptr) :
        m_store(store),
        m_next(next)
    {
    }

    virtual ~Dispatcher(){}

    void set_store(Store *store) {
        m_store = store;
    }

    Dispatcher* get_next() {
        return m_next;
    }

    void set_next(Dispatcher *next) {
        m_next = next;
    }

    virtual void dispatch(const Action action) = 0;

protected:
    Store *m_store;
    Dispatcher *m_next;
};

// List of Dispatchers to be applied as middleware
typedef QList<Dispatcher*> MiddlewareList;


} // namespace Redux
