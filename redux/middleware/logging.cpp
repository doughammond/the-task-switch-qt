//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// STL
#include <iostream>

// Qt
#include <QJsonDocument>

// redux
#include "middleware/logging.h"


namespace Redux {

LoggingMiddleware::LoggingMiddleware(Store *store) :
    Dispatcher(store)
{

}

void LoggingMiddleware::dispatch(const Action action)
{
    // All we do is dump the action to qDebug and then next dispatch
    const std::string at = action.first.toStdString();
    QJsonDocument jpm = QJsonDocument::fromVariant(action.second);
    const std::string jpt = jpm.toJson().toStdString();
    std::cout << "redux : dispatched action :\n\t" << at << "\n\t" << jpt << std::endl;
    m_next->dispatch(action);
}

} // namespace Redux
