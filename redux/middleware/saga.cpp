//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// redux
#include "middleware/saga.h"

namespace Redux {

SagaMiddleware::SagaMiddleware(Store *store):
    Dispatcher(store)
{

}

void SagaMiddleware::dispatch(const Action action)
{
    SagaList::iterator si = m_sagas.begin();
    while (si != m_sagas.end()) {
        Saga *s = *si;
        SagaActionTaskList satl = s->get_action_tasks();
        SagaActionTaskList::iterator ati = satl.begin();
        while (ati != satl.end()) {
            SagaActionTaskPair satp = *ati;
            if (action.first == satp.first) {
                ActionList al = satp.second->run(action, m_store);
                ActionList::iterator ai = al.begin();
                while (ai != al.end()) {
                    Action na = *ai;
                    m_next->dispatch(na);
                    ++ai;
                }
            }
            ++ati;
        }
        ++si;
    }
    m_next->dispatch(action);
}


} // namespace Redux
