//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// redux
#include "dispatcher.h"


namespace Redux {

// Saga Tasks return lists of actions to be re-dispatched
typedef QList<Action> ActionList;

// SagaTask is the smallest unit of side-effect management;
// should be used to accept one action type, and return
// multiple others
class SagaTask
{
public:
    virtual ActionList run(const Action action, Store *store) = 0;
};

typedef QPair<QString, SagaTask*> SagaActionTaskPair;
typedef QList<SagaActionTaskPair> SagaActionTaskList;

// Saga holds a collection of SagaTasks, and is able to
// filter the dispached actions into SagaTasks.
class Saga
{
public:
    void add_action_task(QString actionType, SagaTask *task) {
        SagaActionTaskPair satp(actionType, task);
        m_tasks.push_back(satp);
    }

    SagaActionTaskList get_action_tasks() {
        return m_tasks;
    }

private:
    SagaActionTaskList m_tasks;
};

typedef QList<Saga*> SagaList;


// SagaMiddleware is the controlling Dispatcher for
// multiple Sagas, which receives back the generated
// side-effects and re-dispacthes them to the next
// dispatcher
class SagaMiddleware : public Dispatcher
{
public:
    SagaMiddleware(Store *store);

    void dispatch(const Action action);

    void add_saga(Saga *saga) {
        m_sagas.push_back(saga);
    }

private:
    SagaList m_sagas;
};


} // namespace Redux
