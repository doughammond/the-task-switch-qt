//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// #include <QDebug>
#include <QElapsedTimer>
#include <utility>

#include "store.h"

namespace Redux {

Action makeAction(QString type, ActionProperties props) {
    Action a;
    a.first = std::move(type);
    a.second = std::move(props);
    return a;
}


Store::Store(Reducer *r, QObject *parent)
    : QObject(parent),
      m_dispatcher(new SingleDispatch(
          this
      )),
      m_reducer(r)
{
    // Ensure we have a default state
    ActionProperties ap;
    Action ac = makeAction("", ap);
    dispatch(ac);
}

void Store::apply_middleware(const MiddlewareList& mw)
{
    Dispatcher *d = m_dispatcher;
    MiddlewareList::const_iterator ri = mw.constEnd();
    while (ri != mw.constBegin()) {
        --ri;
        Dispatcher *m = *ri;
        m->set_next(d);
        m->set_store(this);
        d = m;
    }
    m_dispatcher = d;
}



SingleDispatch::SingleDispatch(Store *store) : Dispatcher(store)
{

}

void SingleDispatch::dispatch(const Action action)
{
//    QElapsedTimer t;
//    t.start();

    Redux::State new_state = m_store->get_reducer()->reduce(
        action, m_store->get_state()
    );
    m_store->set_state(new_state);

    // qDebug() << "Redux single dispatch took " << (t.nsecsElapsed() / 1.0e6) << "ms";
}


} // namespace Redux
