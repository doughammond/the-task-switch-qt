//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>

// redux
#include "types.h"
#include "reducer.h"
#include "dispatcher.h"

namespace Redux {


Action makeAction(QString type, ActionProperties props);


class Store : public QObject
{
    Q_OBJECT

public:

    Store(Reducer *r, QObject *parent = nullptr);

    void apply_middleware(const MiddlewareList& mw);

    Reducer* get_reducer() {
        return m_reducer;
    }

    State get_state() {
        return m_state;
    }

    void set_state(const State state) {
        m_state = state;
        emit state_changed(m_state);
    }

    void dispatch(const Action action) {
        return m_dispatcher->dispatch(action);
    }

signals:
    void state_changed(const State newstate);

private:
    Dispatcher *m_dispatcher;
    Reducer *m_reducer;

    State m_state;
};



// SingleDispatch is the default/root dispatch mechanism for Store
class SingleDispatch : public Dispatcher {
public:
    SingleDispatch(Store *store);

    void dispatch(const Action action);
};

} // namespace Redux
