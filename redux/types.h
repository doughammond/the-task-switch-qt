//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// std
#include <functional>

// Qt
#include <QObject>
#include <QVariant>


namespace Redux {

class Store; // fwd decl

// Types for the top-level state store
typedef QVariantMap State;

// Types for constructing actions
typedef QVariantMap ActionProperties;
typedef QPair<QString, ActionProperties> Action;


} // namespace Redux
