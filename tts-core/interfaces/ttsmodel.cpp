//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "ttsmodel.h"


IModel::IModel(TTSValidator *validator, QObject *parent)
    : QObject(parent),
      m_validator(validator)
{

}


void IModel::on_submit(const TaskSubmission task)
{
    ValidationResult r(true, "");
    if (m_validator) {
        r = m_validator->validate(task.first, task.second);
        if (r.first) {
            r = do_submit(task);
        }
    } else {
        r = do_submit(task);
    }
    emit submit_result(task, r.first, r.second);
}
