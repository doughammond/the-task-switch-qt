//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
// #include <QDebug>
#include <QObject>

// tts-core
#include "types.h"
#include "validation.h"


class IModel : public QObject
{
    Q_OBJECT

public:
    IModel(TTSValidator *validator = nullptr, QObject *parent = nullptr);

    const QString get_user_tag(const quint8 tagindex) {
        if (tagindex < m_user_tags.size()) {
            // qInfo() << "get tag at index " << tagindex;
            return m_user_tags[tagindex];
        }
        return "";
    }

public slots:
    virtual void start() = 0;
    virtual void get_last_task() = 0;
    virtual void on_submit(const TaskSubmission task);

    void emit_ready() {
        emit ready();
    }

    void emit_pause() {
        emit pause();
    }

    void emit_model_status(const QString status) {
        emit model_status(status);
    }

    void emit_submit_result(const TaskSubmission task, const bool result, const QString reason) {
        emit submit_result(task, result, reason);
    }

    void emit_get_last_item_result(const TaskSubmission task) {
        emit get_last_item_result(task);
    }

    void emit_sync_active(const bool active) {
        emit sync_active(active);
    }

    void emit_sync_status(const bool result, const QString reason) {
        emit sync_status(result, reason);
    }

    void emit_get_user_tags_result(const TagList tags) {
        emit get_user_tags_result(tags);
    }

signals:
    void ready();
    void pause();
    void model_status(const QString status);
    void submit_result(const TaskSubmission task, const bool result, const QString reason);
    void get_last_item_result(const TaskSubmission task);
    void sync_active(const bool active);
    void sync_status(const bool result, const QString reason);
    void get_user_tags_result(const TagList tags);

protected:
    TTSValidator *m_validator;
    TagList m_user_tags;

private:
    virtual ValidationResult do_submit(const TaskSubmission task) = 0;
};
