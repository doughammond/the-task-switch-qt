//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QList>

// tts-core
#include "ttsmodel.h"


// The buffer of text being shown on the UI display
typedef QStringList DisplayList;


class IView : public QObject
{
    Q_OBJECT

public:
    IView(QObject *parent = nullptr);

    virtual void show() = 0;

public slots:
    virtual void on_update_display(const DisplayList dsp) = 0;
    virtual void on_action_status(const bool has_task, const bool has_tags) = 0;
    virtual void on_update_selected_tags(const TagSet tags) = 0;
    virtual void on_status_change(const QString status) = 0;
    virtual void on_get_user_tags(const TagList tags) = 0;

signals:
    void action_button_press();
    void tag_button_press(const quint8 tagindex);

protected:
    TagList m_user_tags;
};


// List of views in use
typedef QList<IView*> ViewList;
