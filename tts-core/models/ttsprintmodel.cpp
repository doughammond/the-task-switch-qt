//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <iostream>

// tts-core
#include "ttsprintmodel.h"

TTSPrintModel::TTSPrintModel(TTSValidator *validator, QObject *parent) : IModel(validator, parent)
{
}

void TTSPrintModel::start()
{
    emit_ready();
}

ValidationResult TTSPrintModel::do_submit(const TaskSubmission task)
{
    const QStringList tl = task.second.values();
    const std::string td = task.first.toString().toStdString();
    const std::string tt = tl.join(", ").toStdString();
    std::cout << "TASK: " << td << " / " << tt << std::endl;
    ValidationResult r(true, "");
    return r;
}
