//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

#include <QObject>

#include <interfaces/ttsmodel.h>

class TTSPrintModel : public IModel
{
    Q_OBJECT

public:
    TTSPrintModel(TTSValidator *validator = nullptr, QObject *parent = nullptr);
    ~TTSPrintModel() {}

public slots:
    void start();
    ValidationResult do_submit(const TaskSubmission task);

signals:
    void submit_result(const TaskSubmission task, const bool result, const QString reason);
};
