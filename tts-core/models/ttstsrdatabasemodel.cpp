//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariant>

#include "models/ttstsrdatabasemodel.h"

constexpr char defaultServiceUrl[] = "http://localhost:8118";

TTSTSRDatabaseModel::TTSTSRDatabaseModel(TTSValidator *validator, QObject *parent)
    : IModel(validator, parent)
{
    connect(
        this, &TTSTSRDatabaseModel::ready,
        this, [=]() {
            get_user_tags([=]() {
                get_last_task();
            });
        }
    );
}

void TTSTSRDatabaseModel::get_last_task()
{
    const auto exec = [=](){
        QNetworkRequest request(QUrl(m_url + "/api/v2/timesheet_item/latest"));
        request.setRawHeader("Authorization", m_access_token.toLocal8Bit());
        // qInfo() << "making net request to " << request.url();
        emit_sync_active(true);
        QNetworkReply *reply = m_netman.get(request);
        connect(
            reply, &QNetworkReply::finished,
            this, [=]() {
                reply->deleteLater();
                if (reply->isFinished() && reply->error() == QNetworkReply::NoError) {
                    // qInfo() << "have finished net reply from " << reply->url();
                    QJsonDocument jd = QJsonDocument::fromJson(reply->readAll());
                    QDateTime td = QDateTime::fromString(jd["time_started"].toString(), Qt::ISODate);
                    QVariantList tvl = jd["categories"].toArray().toVariantList();
                    QStringList tsl;
                    std::for_each(
                        tvl.begin(), tvl.end(),
                        [&tsl](QVariant v) {
                            tsl << v.toMap()["name"].toString();
                        }
                    );
                    TagSet ts = TagSet::fromList(tsl);
                    //TagSet ts = TagSet(tsl.begin(), tsl.end());
                    // qInfo() << "  " << td << " : " << ts;
                    const TaskSubmission t{td, ts};
                    emit_get_last_item_result(t);
                    // qInfo() << "  done";
                } else if (reply->isFinished() && reply->error() != QNetworkReply::NoError) {
                    QString errm = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString() + " " + reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
                    qWarning() << "last task request failed " << errm;
                    emit_sync_status(false, "Update failed");
                }
                emit_sync_active(false);
            }
        );
    };
    check_token(exec);
}

void TTSTSRDatabaseModel::get_user_tags(voidCallback callback)
{
    const auto exec = [=]() {
        QNetworkRequest request(QUrl(m_url + "/api/v2/user/categories"));
        request.setRawHeader("Authorization", m_access_token.toLocal8Bit());
        // qInfo() << "making net request to " << request.url() << " with auth " << m_access_token;
        emit_sync_active(true);
        QNetworkReply *reply = m_netman.get(request);
        emit_model_status("Fetching tags");
        connect(
            reply, &QNetworkReply::finished,
            this, [=]() {
                reply->deleteLater();
                if (reply->isFinished() && reply->error() == QNetworkReply::NoError) {
                    qInfo() << "have finished net reply from " << reply->url();
                    QJsonDocument jd = QJsonDocument::fromJson(reply->readAll());
                    QJsonArray ja = jd.array();
                    qInfo() << "  " << ja;

                    m_user_tags.clear();
                    QJsonArray::const_iterator jai = ja.constBegin();
                    while (jai != ja.constEnd()) {
                        QJsonValue jo = *jai;
                        QJsonValue jon = jo["category"]["name"];
                        QString cs = jon.toString();
                        qInfo()  << "    " << cs;
                        m_user_tags.append(cs);

                        ++jai;
                    }

                    qInfo() << "  " << m_user_tags;
                    emit_get_user_tags_result(m_user_tags);
                    callback();
                } else if (reply->isFinished() && reply->error() != QNetworkReply::NoError) {
                    qInfo() << "err: " << reply->error();
                    qInfo() << "res: " << reply->readAll();
                    QString errm = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString() + " " + reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
                    qWarning() << "user tags request failed with status: " << errm << "retry in 5s";
                    emit_sync_status(false, errm);
                    if (reply->error() == QNetworkReply::AuthenticationRequiredError) {
                        new_login();
                    } else {
                        QTimer::singleShot(5000, [=](){
                            get_user_tags(callback);
                        });
                    }
                }
                emit_sync_active(false);
            }
        );
    };
    check_token(exec);
}

void TTSTSRDatabaseModel::start()
{
    QSettings s;
    m_url = s.value("serviceUrl", defaultServiceUrl).toString();
    s.setValue("serviceUrl", m_url);

    do_login();
}

void TTSTSRDatabaseModel::on_submit(const TaskSubmission task)
{
    const auto exec = [=]() {
        qDebug() << "on_submit" << task;
        ValidationResult r(true, "");
        if (m_validator) {
            r = m_validator->validate(task.first, task.second);
            if (!r.first) {
                emit_submit_result(task, false, r.second);
                return;
            }
        }

        QJsonObject jo;
        jo.insert("time_started", task.first.toString(Qt::ISODate));
        QJsonArray ta = QJsonArray::fromStringList(task.second.values());
        jo.insert("categories", ta);
        QJsonDocument jd;
        jd.setObject(jo);

        qDebug() << " submit json doc " << jd.toJson();

        QNetworkRequest request(QUrl(m_url + "/api/v2/timesheet_item"));
        request.setRawHeader("Authorization", m_access_token.toLocal8Bit());
        request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));
        // qInfo() << "making net request to " << request.url();
        emit_sync_active(true);
        QNetworkReply *reply = m_netman.put(request, jd.toJson());
        connect(
            reply, &QNetworkReply::finished,
            [=]() {
                reply->deleteLater();
                if (reply->isFinished() && reply->error() == QNetworkReply::NoError) {
                    // qInfo() << "have finished net reply from " << reply->url();
                    emit_submit_result(task, true, "");
                } else if (reply->isFinished() && reply->error() != QNetworkReply::NoError) {
                    QString errm = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString() + " " + reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
                    qWarning() << "submit request failed " << errm;
                    emit_sync_status(false, errm);
                    emit_submit_result(task, false, "Submit failed");
                }
                emit_sync_active(false);
            }
        );
    };
    check_token(exec);
}

void TTSTSRDatabaseModel::do_login()
{
    QSettings s;
    const auto v_access_token = s.value("access_token");
    if (v_access_token.isValid()) {
        m_access_token = v_access_token.toString();
        m_refresh_token = s.value("refresh_token").toString();
        m_token_expiry = s.value("token_expiry").toDateTime();
        qDebug() << "using stored auth tokens";

        check_token([=](){
            emit_ready();
        });
        return;
    } else {
        new_login();
    }
}

void TTSTSRDatabaseModel::new_login()
{
    // do the device code auth ritual
    QNetworkRequest urlRequest(QUrl(m_url + "/auth-url"));
    QNetworkReply *urlReply = m_netman.get(urlRequest);
    connect(
        urlReply, &QNetworkReply::finished,
        [=]() {
            if (urlReply->isFinished() && urlReply->error() != QNetworkReply::NoError) {
                emit_model_status("Login URL Failed");
                // Retry
                QTimer::singleShot(5000, this, &TTSTSRDatabaseModel::do_login);
                return;
            }
            QJsonDocument jd = QJsonDocument::fromJson(urlReply->readAll());
            m_paircode = jd["state"].toString();
            const auto url = jd["url"].toString();
            qDebug() << "auth url is" << url;
            emit_model_status(url);
            QTimer::singleShot(5000, this, &TTSTSRDatabaseModel::poll_login);
        }
    );
}

void TTSTSRDatabaseModel::poll_login()
{
    QJsonObject ro;
    ro.insert("state", m_paircode);
    QJsonDocument rd;
    rd.setObject(ro);

    QNetworkRequest codeRequest(QUrl(m_url + "/auth-code"));
    codeRequest.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));
    QNetworkReply *codeReply = m_netman.post(codeRequest, rd.toJson());
    connect(
        codeReply, &QNetworkReply::finished,
        [=]() {
            if (codeReply->isFinished() && codeReply->error() != QNetworkReply::NoError) {
                qDebug() << "no auth code response; will retry";
                QTimer::singleShot(5000, this, &TTSTSRDatabaseModel::poll_login);
                return;
            }
            QJsonDocument jd = QJsonDocument::fromJson(codeReply->readAll());
            store_tokens(jd["access_token"].toString(), jd["refresh_token"].toString(), jd["expires_at"].toString());
            emit_ready();
        }
    );
}

void TTSTSRDatabaseModel::check_token(voidCallback callback)
{
    // TODO: it's recommended to refresh if the time left is < 5 mins
    QDateTime now = QDateTime::currentDateTimeUtc();
    // qDebug() << "comparing token expiry" << now << "<" << m_token_expiry;
    if (m_token_expiry < now || m_access_token.length() == 0) {
        qDebug() << "tokens expired";
        refresh_token(callback);
    } else {
        // qDebug() << "tokens not expired";
        callback();
    }
}

void TTSTSRDatabaseModel::refresh_token(voidCallback callback, int retry)
{
    qDebug() << "refresh tokens";
    QNetworkRequest request(QUrl(m_url + "/auth-refresh"));
    request.setRawHeader("Authorization", m_refresh_token.toLocal8Bit());
    request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));
    QNetworkReply *reply = m_netman.post(request, "");
    connect(
        reply, &QNetworkReply::finished,
        [=]() {
            reply->deleteLater();
            if (reply->isFinished()) {
                if (retry <= 5 && reply->error() != QNetworkReply::NoError) {
                    emit_model_status("Refresh failed");
                    // Retry
                    QTimer::singleShot(5000, this, [=]() {
                        refresh_token(callback, retry + 1);
                    });
                } else {
                    qDebug() << "request error or ran out of refresh attempts; require new login";
                    emit_pause();
                    new_login();
                }
                return;
            }
            const auto data = reply->readAll();
            QJsonDocument jd = QJsonDocument::fromJson(data);
            // the refresh_token never changes!
            store_tokens(jd["access_token"].toString(), m_refresh_token, jd["expires_at"].toString());
            callback();
        }
    );
}

void TTSTSRDatabaseModel::store_tokens(const QString &access_token, const QString &refresh_token, const QString &expires_at)
{
    m_access_token = access_token;
    // qDebug() << "access_token is" << m_access_token;
    m_refresh_token = refresh_token;
    // qDebug() << "refresh_token is" << m_refresh_token;
    m_token_expiry = QDateTime::fromString(expires_at, Qt::ISODate);
    // qDebug() << "expires_at is" << expires_at;
    qDebug() << "Got new tokens; token_expiry is" << m_token_expiry;
    QSettings s;
    s.setValue("access_token", m_access_token);
    s.setValue("refresh_token", m_refresh_token);
    s.setValue("token_expiry", m_token_expiry);
}
