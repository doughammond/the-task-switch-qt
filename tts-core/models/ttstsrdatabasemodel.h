//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

#include <functional>

typedef std::function<void(void)> voidCallback;

#include <QObject>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <interfaces/ttsmodel.h>

class TTSTSRDatabaseModel : public IModel
{
    Q_OBJECT

public:
    explicit TTSTSRDatabaseModel(TTSValidator *validator = nullptr,
        QObject *parent = nullptr
    );

    void get_last_task();

    void get_user_tags(voidCallback callback);

public slots:
    void start();
    void on_submit(const TaskSubmission task);

private:
    void do_login();
    void new_login();
    void poll_login();
    void check_token(voidCallback callback);
    void refresh_token(voidCallback callback, int retry = 0);
    void store_tokens(const QString &access_token, const QString &refresh_token, const QString &expires_at);

    ValidationResult do_submit(const TaskSubmission) {
        return {false, "not implemented"};
    }

    QString m_url;
    QNetworkAccessManager m_netman;
    QString m_access_token;
    QString m_refresh_token;
    QDateTime m_token_expiry;
    QString m_paircode;
};
