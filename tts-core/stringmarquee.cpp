// #include <QDebug>

#include "stringmarquee.h"

StringMarquee::StringMarquee(QObject *parent) : QObject(parent)
{
    connect(
        &m_timer, &QTimer::timeout,
        this, &StringMarquee::on_timer
    );
}

void StringMarquee::on_update_display(const DisplayList dsp)
{
    m_timer.stop();

    const auto dsize = dsp.size();
    if (dsize != m_input.size()) {
        m_input.clear();
        m_enabled.clear();
        m_size.clear();
        m_ptr.clear();
        m_output.clear();

        for (int i = 0; i < dsize; ++i) {
            m_input.push_back("");
            m_enabled.push_back(false);
            m_size.push_back(0);
            m_ptr.push_back(0);
            m_output.push_back("");
        }
    }

    bool anychange = false;

    for (int i = 0; i < dsize; ++i) {
        const auto line = dsp[i];
        const size_t size = line.size();
        const auto enabled = line.size() > 16;
        const auto next = enabled
            ? line + "  " + line + "  "
            : line;
        m_size[i] = enabled
            ? size + 2
            : size;
        if (next != m_input[i]) {
            m_input[i] = next;
            m_ptr[i] = 0;
            m_enabled[i] = enabled;
            anychange = true;
        }
    }

    // update now
    if (anychange) {
        on_timer();
    }
    // and in the future
    m_timer.start(1000);
}

void StringMarquee::on_timer()
{
    m_output.clear();

    for (int i = 0; i < m_input.size(); ++i) {
        // qDebug() << "M: " << i << m_enabled[i] << m_size[i] << m_ptr[i];

        const auto part = m_enabled[i]
            ? m_input[i].mid(m_ptr[i], 16)
            : m_input[i];
        m_output.push_back(part);
        m_ptr[i] = m_size[i] > 0
            ? (m_ptr[i] + 1) % m_size[i]
            : 0;
    }

    emit update_display(m_output);
}
