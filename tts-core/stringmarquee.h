#pragma once

#include <QObject>
#include <QTimer>

#include <interfaces/ttsview.h>

/**
 * @brief The StringMarquee class
 * This is a View middleware, which intercepts strings being
 * sent to the display, and performs a scrolling marquee effect
 * so that strings longer than 16 chars per line can be viewed.
 */
class StringMarquee : public QObject
{
    Q_OBJECT
public:
    explicit StringMarquee(QObject *parent = nullptr);

public slots:
    void on_update_display(const DisplayList dsp);

signals:
    void update_display(const DisplayList &dsp);

private:
    void on_timer();

    QTimer m_timer;

    DisplayList m_input;
    QList<bool> m_enabled;
    QList<size_t> m_size;
    QList<size_t> m_ptr;

    DisplayList m_output;
};
