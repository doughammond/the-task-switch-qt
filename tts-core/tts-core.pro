#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

QT       -= gui
QT       += network

CONFIG += c++14

TARGET = tts-core
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    interfaces/ttsmodel.cpp \
    interfaces/ttsview.cpp \
    stringmarquee.cpp \
    ttscontroller.cpp \
    models/ttsprintmodel.cpp \
    models/ttstsrdatabasemodel.cpp \
    validation.cpp

HEADERS += \
    interfaces/ttsmodel.h \
    interfaces/ttsview.h \
    stringmarquee.h \
    ttscontroller.h \
    models/ttsprintmodel.h \
    models/ttstsrdatabasemodel.h \
    validation.h \
    types.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}


# static linking redux
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../redux/release/ -lredux
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../redux/debug/ -lredux
else:unix: LIBS += -L$$OUT_PWD/../redux/ -lredux

INCLUDEPATH += $$PWD/../redux
DEPENDPATH += $$PWD/../redux

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/release/libredux.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/debug/libredux.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/release/redux.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/debug/redux.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../redux/libredux.a

# # YAML
# unix: CONFIG += link_pkgconfig
# unix: PKGCONFIG += yaml-cpp
