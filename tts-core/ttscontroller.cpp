//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// std
#include <iostream>

// Qt
#include <QDebug>
#include <QElapsedTimer>

// redux
#include <reducers/null.h>

// tts-core
#include "ttscontroller.h"
#include "validation.h"


DisplayList display_state(Redux::State state)
{
    QVariantMap dspv = state["display"].toMap();
    DisplayList dsp = dspv["value"].toStringList();
    return dsp;
}

DisplayList format_display_text(DisplayList dsp, const uint row, const uint col, const QString& text)
{
//    QElapsedTimer t;
//    t.start();

    // ensure we are writing text within the bounds of the buffer
    if (row > 1 || col > 15) {
        return dsp;
    }

    QString row_text = dsp[row];
    // ensure the row text is fully filled, then truncate
    // to the display length
    row_text.append("                ");
    row_text.truncate(16);
    // replace does not exceed the source string length
    row_text.replace(col, text.size(), text);

    // update the display list
    dsp[row] = row_text;

    // qDebug() << "format_display_text took " << (t.nsecsElapsed() / 1.0e6) << "ms";
    return dsp;
}

QString seconds_to_hhmm(const qint64 seconds)
{
    const qint64 DAY = 86400;
    long hrs = seconds / 3600;
    QTime t = QTime(0,0).addSecs(seconds % DAY);
    long mns = t.minute();
    return QString("%1:%2")
        .arg(hrs, 2, 10, QChar('0'))
        .arg(mns, 2, 10, QChar('0'));
}




Redux::State DisplayReducer::reduce(const Redux::Action action, const Redux::State state)
{
    Redux::State next_state(state);
    DisplayList dsp = next_state["value"].toStringList();

    // ensure we have 2 rows to play with, in case
    // this is the first time the reducer has run.
    if (dsp.size() < 2) {
        dsp.push_back("");
        dsp.push_back("");
    }

    if (action.first == "SET_DISPLAY_TEXT") {
        dsp = format_display_text(
            dsp,
            action.second["row"].toUInt(),
            action.second["col"].toUInt(),
            action.second["text"].toString()
        );
    }

    next_state["value"] = dsp;
    return next_state;
}

Redux::State TagsReducer::reduce(const Redux::Action action, const Redux::State state)
{
    Redux::State nstate(state);
    QVariant next_state = nstate["value"];

    if (action.first == "CLEAR_TAGS") {
        return Redux::State();
    }

    if (action.first == "TOGGLE_TAG") {
        auto tags = qvariant_cast<TagSet>(next_state);
        QString tag = action.second["tag"].toString();
        if (tags.contains(tag)) {
            tags.remove(tag);
        } else {
            tags.insert(tag);
        }
        if (!tags.empty()) {
            next_state.setValue(tags);
        } else {
            return Redux::State();
        }
    }

    nstate["value"] = next_state;
    return nstate;
}

Redux::State StatusReducer::reduce(const Redux::Action action, const Redux::State state)
{
    Redux::State next_state(state);

    if (action.first == "SET_STATUS") {
        next_state["value"] = action.second["status"].toString();
    }

    return next_state;
}

Redux::State TaskReducer::reduce(const Redux::Action action, const Redux::State state)
{
    Redux::State nstate(state);
    QVariant next_state = nstate["value"];

    if (action.first == "SET_TASK") {
        auto task = qvariant_cast<TaskSubmission>(action.second["task"]);
        next_state.setValue(task);
    }

    if (action.first == "CLEAR_TASK") {
        return Redux::State();
    }

//    // --- TESTING ---
//    TagSet ts;
//    ts.insert("FOO"); ts.insert("BAR");
//    QDateTime td;
//    td.setDate(QDate(2018, 5, 14));
//    td.setTime(QTime(10, 15));
//    TaskSubmission tt(td, ts);
//    next_state.setValue(tt);
//    // --- TESTING ---

    nstate["value"] = next_state;
    return nstate;
}

TTSReducer::TTSReducer() :
    m_display(new DisplayReducer()),
    m_tags(new TagsReducer()),
    m_status(new StatusReducer()),
    m_task(new TaskReducer())
{
}

Redux::State TTSReducer::reduce(const Redux::Action action, const Redux::State state)
{
    Redux::State next_state(state);
    next_state["display"] = m_display->reduce(action, state["display"].toMap());
    next_state["tags"] = m_tags->reduce(action, state["tags"].toMap());
    next_state["status"] = m_status->reduce(action, state["status"].toMap());
    next_state["task"] = m_task->reduce(action, state["task"].toMap());
    return next_state;
}



TTSController::TTSController(
        IModel *model,
        ViewList views,
        QObject *parent
    ) : QObject(parent),
        m_model(model),
        m_views(views),
        m_store(new Redux::Store(
            new TTSReducer(),
            this
        )),
        m_clock(new QTimer(this)),
        m_temp_tmr(new QTimer(this)),
        m_sync_timer(new QTimer(this))
{
    // connect up the views signals
    ViewList::iterator vi = views.begin();
    while (vi != views.end()) {
        IView *v = *vi;
        connect_view(v);
        ++vi;
    }

    // initalise the state store
    Redux::MiddlewareList mw;
//    mw.push_back(new Redux::LoggingMiddleware(m_store));
    m_store->apply_middleware(mw);
    connect(
        m_store, &Redux::Store::state_changed,
        this, &TTSController::new_state
    );

    // initialise the store
    Redux::ActionProperties ap;
    m_store->dispatch(Redux::makeAction("STARTUP", ap));

    // controller display is not directly connected to views;
    // Use the Marquee middleware:
    connect(
        this, &TTSController::update_display,
        &m_marquee, &StringMarquee::on_update_display
    );

    // connections to the model
    connect(
        m_model, &IModel::ready,
        this, &TTSController::on_model_ready
    );
    connect(
        m_model, &IModel::model_status,
        this, &TTSController::on_model_status
    );
    connect(
        m_model, &IModel::submit_result,
        this, &TTSController::on_submit_result
    );
    connect(
        m_model, &IModel::get_last_item_result,
        this, &TTSController::on_last_task_result
    );
    connect(
        m_model, &IModel::sync_status,
        this, &TTSController::on_sync_status
    );
    connect(
        m_model, &IModel::sync_active,
        this, &TTSController::on_sync_active
    );
    connect(
        this, &TTSController::submit,
        m_model, &IModel::on_submit
    );

    m_model->start();
}

void TTSController::new_state(const Redux::State& state)
{
    emit update_display(display_state(state));

    const Redux::State state_tags = state["tags"].toMap();
    // qInfo() << "have new state tags" << state_tags;
    auto tags = qvariant_cast<TagSet>(state_tags["value"]);
    emit update_selected_tags(tags);

    const Redux::State state_task = state["task"].toMap();
    bool isEnd = false;
    const QVariant taskv = state_task["value"];
    if (!taskv.isNull()) {
        auto task = qvariant_cast<TaskSubmission>(taskv);
        // qInfo() << "have current task" << task;
        isEnd = task.second.size() == 1 && task.second.contains("END");
    }
    emit action_status(!state_task["value"].isNull() && !isEnd, !tags.empty());

    const Redux::State state_status = state["status"].toMap();
    emit status_change(state_status["value"].toString());

    // qInfo()
    //     << "new state "
    //     << "isEnd" << isEnd
    //     << "has_task=" << (!state_task["value"].isNull())
    //     << "tags.size=" << tags.size()
    //     << "tags=" << tags;
}

void TTSController::reset_status()
{
    Redux::ActionProperties ap1;
    ap1["status"] = "normal";
    m_store->dispatch(Redux::makeAction("SET_STATUS", ap1));

    // resume the clock
    m_clock->start();

    // refresh the display
    emit update_display(display_state(m_store->get_state()));
}

/**
 * Action to perform when the clock timer times-out;
 * manage the display - show current time,
 * last submitted info if available,
 * number entry if available
**/
void TTSController::on_clock_timeout()
{
    QDateTime time = QDateTime::currentDateTime();

    // start a new buffer for the display update
    DisplayList dsp;
    dsp.push_back("                ");
    dsp.push_back("                ");

    // update the clock
    dsp = format_display_text(
        dsp,
        1, 11,
        time.toString("HH:mm")
    );

    Redux::State state = m_store->get_state();

    // if there is a last submission, update the
    // elapsed time and display the tags
    Redux::State state_task = state["task"].toMap();
    QVariant taskv = state_task["value"];
    if (!taskv.isNull()) {
        auto task = qvariant_cast<TaskSubmission>(taskv);
        QDateTime submission_time = task.first.toLocalTime();
        QStringList submission_tags = task.second.values();
        const bool isEnd = submission_tags.size() == 1 && submission_tags.contains("END");
        // qInfo() << "update display for tags" << submission_tags << " isEnd=" << isEnd;
        if (!isEnd) {
            dsp = format_display_text(
                dsp,
                0, 0,
                submission_tags.join(" ")
            );
        }
        qint64 diff_seconds = submission_time.secsTo(time);
        QString hhmm = seconds_to_hhmm(diff_seconds);
        QString idleindicator = isEnd ? "-" : "";
        dsp = format_display_text(
            dsp,
            1, 0,
            idleindicator + hhmm
        );
    }

    // push the text buffer to the display state
    set_display_text(0, 0, dsp[0]);
    set_display_text(1, 0, dsp[1]);
}

void TTSController::on_model_ready()
{
    // Set up the timer which refreshes the clock and other display info
    m_clock->setInterval(5000);
    connect(
        m_clock, &QTimer::timeout,
        this, &TTSController::on_clock_timeout
    );
    m_clock->start();

    // Configure temp status timer
    m_temp_tmr->setSingleShot(true);
    m_temp_tmr->setInterval(2000);
    connect(
        m_temp_tmr, &QTimer::timeout,
        this, &TTSController::reset_status
    );

    m_sync_timer->setInterval(30000);
    connect(
        m_sync_timer, &QTimer::timeout,
        this, [=](){
            m_model->get_last_task();
        }
    );
    m_sync_timer->start();

    // update the display immediately after startup
    on_clock_timeout();
}

void TTSController::on_model_status(const QString status)
{
    DisplayList dsp;
    dsp.push_back("                ");
    dsp.push_back("                ");
    dsp = format_display_text(dsp, 0, 0, status);
    set_display_text(0, 0, dsp[0]);
    set_display_text(1, 0, dsp[1]);
}

/**
 * Action to perform on receiving the result of
 * a submission; display error, reset input,
 *  stash the last submission data etc.
**/
void TTSController::on_submit_result(const TaskSubmission task, const bool result, const QString& reason)
{
    qDebug() << "on_submit_result" << task << result << reason;
    if (!result) {
        set_temporary_status("error", reason);
    }  else {
        reset_status();
        reset_tag_selection();

        // if the tag was END then remove the stashed last submission,
        // else store the submission
        TagSet tags = task.second;
        if (tags.contains("END")) {
            Redux::ActionProperties ap1;
            m_store->dispatch(Redux::makeAction("CLEAR_TASK", ap1));
        } else {
            Redux::ActionProperties ap2;
            QVariant taskv;
            taskv.setValue(task);
            ap2["task"] = taskv;
            m_store->dispatch(Redux::makeAction("SET_TASK", ap2));
        }

        // this updates the last submission display
        on_clock_timeout();
        set_temporary_status("success", "OK");
    }
}

/**
 * Handler for the "action" button; can perform
 * one of many actions depending on the current state
**/
void TTSController::on_action_button_press()
{
    qDebug() << "action button press";
    // any input needs to put us back in normal state
    reset_status();

    // the action button can do mutliple things,
    // depending on the current state
    //
    //       | task           | !task
    // ------+----------------+-------------------
    //  tags | pending entry  | pending entry
    //       | active task    | idle
    //       | -> switch task | -> start task
    // ------+----------------+-------------------
    // !tags | no entry       | no entry
    //       | active task    | idle
    //       | -> END task    | -> no action
    // ------+----------------+-------------------
    //
    // There is no way to clear all the selected tags at once

    Redux::State state = m_store->get_state();
    Redux::State state_task = state["task"].toMap();
    QVariant taskv = state_task["value"];

    Redux::State state_tags = state["tags"].toMap();
    QVariant tagsv = state_tags["value"];

    TaskSubmission ts;
    ts.first = QDateTime::currentDateTime();
    if (!tagsv.isNull()) {
        // switch / start task
        qDebug() << "start task";
        auto tags = qvariant_cast<TagSet>(tagsv);
        ts.second.unite(tags);
        emit submit(ts);
    } else if (!taskv.isNull() && tagsv.isNull()) {
        // END task
        ts.second.insert("END");
        emit submit(ts);
    } else if (taskv.isNull() && tagsv.isNull()) {
        // no action
    }
}

/**
 * Action to perform when a tag key is pressed;
 * Updates the state of currently active tags
 * for submission.
*/
void TTSController::on_tag_button_press(const quint8 tagindex)
{
    // any input needs to put us back in normal state
    reset_status();

    QString tag = m_model->get_user_tag(tagindex);

    if (!tag.isEmpty()) {
        Redux::ActionProperties ap1;
        ap1["tag"] = tag;
        m_store->dispatch(Redux::makeAction("TOGGLE_TAG", ap1));
    }
}

void TTSController::on_sync_status(const bool result, const QString& reason)
{
    if (!result) {
        set_temporary_status("error", reason);
    }
}

void TTSController::on_sync_active(const bool is_active)
{
    Redux::ActionProperties ap;

    if (is_active) {
        ap["status"] = "syncing";
    } else {
        ap["status"] = "normal";
    }

    m_store->dispatch(Redux::makeAction("SET_STATUS", ap));
}

void TTSController::on_last_task_result(const TaskSubmission task)
{
    const QDateTime last_task_ts = task.first;
    if (last_task_ts.isValid()) {
        Redux::ActionProperties ap;
        QVariant last_taskv;
        last_taskv.setValue(task);
        ap["task"] = last_taskv;
        m_store->dispatch(Redux::makeAction("SET_TASK", ap));
        // qInfo() << "sucessfully loaded last task";

        // refresh the display
        on_clock_timeout();
    }
    // else {
    //     qInfo() << "last task was not valid";
    // }
}

void TTSController::connect_view(const IView *view)
{
    connect(
        view, &IView::action_button_press,
        this, &TTSController::on_action_button_press
    );

    connect(
        view, &IView::tag_button_press,
        this, &TTSController::on_tag_button_press
    );

    // controller display is not directly connected to views;
    // Use the Marquee middleware:
    connect(
        &m_marquee, &StringMarquee::update_display,
        view, &IView::on_update_display
    );

    connect(
        this, &TTSController::action_status,
        view, &IView::on_action_status
    );

    connect(
        this, &TTSController::update_selected_tags,
        view, &IView::on_update_selected_tags
    );

    connect(
        this, &TTSController::status_change,
        view, &IView::on_status_change
    );

    connect(
        m_model, &IModel::get_user_tags_result,
        view, &IView::on_get_user_tags
    );
}

void TTSController::set_temporary_status(const QString& status, const QString& reason)
{
    // pause the clock
    m_clock->stop();

    // set the status
    Redux::ActionProperties ap1;
    ap1["status"] = status;
    m_store->dispatch(Redux::makeAction("SET_STATUS", ap1));

    // send an error message
    DisplayList temp_dsp;
    temp_dsp.push_back(""); temp_dsp.push_back("");
    temp_dsp = format_display_text(
        temp_dsp,
        0, 0,
        reason
    );
    emit update_display(temp_dsp);

    // resume normal ops in 2 seconds...
    m_temp_tmr->start();
}

void TTSController::set_display_text(const quint8 row, const quint8 col, const QString& text)
{
    Redux::ActionProperties ap1;
    ap1["row"] = row;
    ap1["col"] = col;
    ap1["text"] = text;
    m_store->dispatch(Redux::makeAction("SET_DISPLAY_TEXT", ap1));
}

void TTSController::reset_tag_selection()
{
    Redux::ActionProperties ap1;
    m_store->dispatch(Redux::makeAction("CLEAR_TAGS", ap1));
}

