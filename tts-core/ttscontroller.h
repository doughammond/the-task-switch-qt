//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QTimer>

// redux
#include <store.h>
#include <reducer.h>
#include <middleware/saga.h>
#include <middleware/logging.h>

// tts-core
#include <interfaces/ttsmodel.h>
#include <interfaces/ttsview.h>

#include "stringmarquee.h"


// Utilities
DisplayList display_state(Redux::State state);

DisplayList format_display_text(DisplayList dsp, const uint row, const uint col, const QString& text);

QString seconds_to_hhmm(const qint64 seconds);


// Redux Reducers

class DisplayReducer : public Redux::Reducer {
public:
    Redux::State reduce(const Redux::Action action, const Redux::State state);
};

class TagsReducer : public Redux::Reducer {
public:
    Redux::State reduce(const Redux::Action action, const Redux::State state);
};

class StatusReducer : public Redux::Reducer {
public:
    Redux::State reduce(const Redux::Action action, const Redux::State state);
};

class TaskReducer : public Redux::Reducer {
public:
    Redux::State reduce(const Redux::Action action, const Redux::State state);
};

class TTSReducer : public Redux::Reducer {
public:
    TTSReducer();

    Redux::State reduce(const Redux::Action action, const Redux::State state);

private:
    DisplayReducer *m_display;
    TagsReducer *m_tags;
    StatusReducer *m_status;
    TaskReducer *m_task;
};


// Controller

class TTSController : public QObject
{
    Q_OBJECT
public:
    explicit TTSController(
            IModel *model,
            ViewList views,
            QObject *parent = nullptr
    );

signals:
    void update_display(const DisplayList dsp);
    void update_selected_tags(const TagSet tags);
    void action_status(const bool has_task, const bool has_tags);
    void status_change(const QString status);
    void submit(const TaskSubmission task);

public slots:
    void new_state(const Redux::State& state);
    void reset_status();
    void on_clock_timeout();
    void on_model_ready();
    void on_model_status(const QString status);
    void on_submit_result(const TaskSubmission task, const bool result, const QString& reason);
    void on_action_button_press();
    void on_tag_button_press(const quint8 tag);
    void on_sync_status(const bool result, const QString& reason);
    void on_sync_active(const bool is_active);
    void on_last_task_result(const TaskSubmission task);

private:
    void connect_view(const IView *view);
    void set_temporary_status(const QString& status, const QString& reason);
    void set_display_text(const quint8 row, const quint8 col, const QString& text);
    void reset_tag_selection();

    // MVC members
    IModel *m_model;
    ViewList m_views;
    Redux::Store *m_store;

    // Other members
    QTimer *m_clock;
    QTimer *m_temp_tmr;
    QTimer *m_sync_timer;

    StringMarquee m_marquee;
};
