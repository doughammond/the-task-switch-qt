//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QDate>
#include <QSet>
#include <QList>
#include <QMap>
#include <QDateTime>

// A set of strings which are the tags on a task;
// or the currently selected tags
typedef QSet<QString> TagSet;

// List of available user tags
typedef QStringList TagList;

// A time-tags pair which represents the start of
// a task characterised by the tags
typedef QPair<QDateTime, TagSet> TaskSubmission;

// Result flag and string reason for validation
typedef QPair<bool, QString> ValidationResult;

// In memory representation of a timesheet line item
// TODO struct use seems out of place, can we use some Qt type ?
// Qt 5 seems to encourage use of std:: types, use std::tuple or std::map (or QMap) ?
struct TimesheetItem
{
    QTime time;
    QString description;
    QString classification;
    TagSet categories;
};

// collection of timesheet line items for a day
typedef QList<TimesheetItem> TimesheetDay;

// collection of timesheet days
typedef QMap<QDate, TimesheetDay> Timesheet;
