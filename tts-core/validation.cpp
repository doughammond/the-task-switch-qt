//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "validation.h"

TTSValidator::TTSValidator(QObject *parent) : QObject(parent)
{

}

ValidationResult TTSRequireTagsValidator::validate(const QDateTime  /*timestamp*/, const TagSet tags)
{
    ValidationResult r(true, "");
    if (tags.empty()) {
        r.first = false;
        r.second = "Need a tag";
    }
    return r;
}


ValidationResult TTSNotOnlyURGIMPValidator::validate(const QDateTime  /*timestamp*/, const TagSet tags)
{
    ValidationResult r(true, "");
    TagSet urg_imp = { "URG", "IMP" };
    TagSet cmp(tags);
    if (cmp.subtract(urg_imp).empty()) {
        r.first = false;
        r.second = "Need more tags";
    }
    return r;
}


ValidationResult TTSSoloENDValidator::validate(const QDateTime  /*timestamp*/, const TagSet tags)
{
    ValidationResult r(true, "");
    if (tags.size() > 1 && tags.contains("END")) {
        r.first = false;
        r.second = "Select only END";
    }
    return r;
}


TTSCombiningValidator::TTSCombiningValidator(TTSValidatorList *children, QObject *parent)
    : TTSValidator(parent),
      m_children(children)
{

}


ValidationResult TTSOrValidator::validate(const QDateTime timestamp, const TagSet tags)
{
    ValidationResult r(true, "");
    TTSValidatorList::const_iterator ci = m_children->constBegin();
    while (ci != m_children->constEnd()) {
        TTSValidator *c = *ci;
        ValidationResult cr = c->validate(timestamp, tags);
        if (cr.first) {
            return r;
        }
        ++ci;
    }
    return r;
}

ValidationResult TTSAndValidator::validate(const QDateTime timestamp, const TagSet tags)
{
    ValidationResult r(true, "");
    TTSValidatorList::const_iterator ci = m_children->constBegin();
    while (ci != m_children->constEnd()) {
        TTSValidator *c = *ci;
        ValidationResult cr = c->validate(timestamp, tags);
        if (!cr.first) {
            return cr;
        }
        ++ci;
    }
    return r;
}
