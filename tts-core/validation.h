//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QDateTime>

// tts-core
#include "types.h"


class TTSValidator : public QObject
{
    Q_OBJECT

public:
    explicit TTSValidator(QObject *parent = nullptr);

    virtual ValidationResult validate(const QDateTime timestamp, const TagSet tags) = 0;
};


class TTSRequireTagsValidator : public TTSValidator {
    Q_OBJECT

public:
    ValidationResult validate(const QDateTime timestamp, const TagSet tags);
};


class TTSNotOnlyURGIMPValidator : public TTSValidator {
    Q_OBJECT

public:
    ValidationResult validate(const QDateTime timestamp, const TagSet tags);
};


class TTSSoloENDValidator : public TTSValidator {
    Q_OBJECT

public:
    ValidationResult validate(const QDateTime timestamp, const TagSet tags);
};


typedef QList<TTSValidator*> TTSValidatorList;

class TTSCombiningValidator : public TTSValidator {
    Q_OBJECT

public:
    explicit TTSCombiningValidator(TTSValidatorList *children, QObject *parent = nullptr);

protected:
    TTSValidatorList *m_children;
};


class TTSOrValidator : public TTSCombiningValidator {
    Q_OBJECT

public:
    TTSOrValidator(TTSValidatorList *children, QObject *parent = nullptr)
        : TTSCombiningValidator(children, parent) {}

    ValidationResult validate(const QDateTime timestamp, const TagSet tags);
};


class TTSAndValidator : public TTSCombiningValidator {
    Q_OBJECT

public:
    TTSAndValidator(TTSValidatorList *children, QObject *parent = nullptr)
        : TTSCombiningValidator(children, parent) {}

    ValidationResult validate(const QDateTime timestamp, const TagSet tags);
};

