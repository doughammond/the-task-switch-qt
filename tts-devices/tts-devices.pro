#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

QT       -= gui

CONFIG += c++14

TARGET = tts-devices
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    tts-max7219.cpp \
    tts-rgblcd.cpp \
    tts-mcp23017.cpp

HEADERS += \
    tts-max7219.h \
    tts-mcp23017.h \
    tts-rgblcd.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

# dynamic linking wiringPi - these are required in the
# project which uses this library; has no effect here
unix|win32: LIBS += -lwiringPi -lwiringPiDev -lrt -lcrypt
