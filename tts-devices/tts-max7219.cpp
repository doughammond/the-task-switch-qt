//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// #include <QDebug>
#include <QElapsedTimer>

#include "tts-max7219.h"

MAX7219::MAX7219(const quint8 ce, const quint8 mosi, const quint8 clk, const quint8 digits, QObject *parent)
    : QObject(parent),
     m_ce(ce),
     m_mosi(mosi),
     m_clk(clk)
{
    pinMode(m_ce, OUTPUT);
    pinMode(m_mosi, OUTPUT);
    pinMode(m_clk, OUTPUT);

    digitalWrite(m_ce, HIGH);
    digitalWrite(m_mosi, LOW);
    digitalWrite(m_clk, LOW);

    send(NO_OP, 0);
    set_scan_limit(digits - 1);
    set_decode_mode(MODE_NONE);
    set_display_test(0);
    set_intensity(0x8);
    normal();
}

bool MAX7219::set_digit(const quint8 digit, const quint8 segments)
{
    quint8 digit_register = digit + 1;
    if (digit_register < 0x01 || digit_register > 0x08) {
        // qInfo() << "digit " << digit << " is not a digit register";
        return false;
    }
    return send(digit_register, segments);
}

bool MAX7219::set_scan_limit(const quint8 digits)
{
    return send(SCAN_LIMIT, digits);
}

bool MAX7219::set_decode_mode(const quint8 mode)
{
    return send(DECODE_MODE, mode);
}

bool MAX7219::set_display_test(const quint8 test)
{
    return send(DISPLAY_TEST, test);
}

bool MAX7219::set_intensity(const quint8 level)
{
    return send(INTENSITY, level);
}

bool MAX7219::normal()
{
    return send(SHUTDOWN, 1);
}

bool MAX7219::shutdown()
{
    return send(SHUTDOWN, 0);
}

bool MAX7219::send_16_bits(const quint16 value)
{
    quint16 mask = 0x8000;
    while (mask != 0) {
        if (value & mask) {
            digitalWrite(m_mosi, HIGH);
        } else {
            digitalWrite(m_mosi, LOW);
        }

        digitalWrite(m_clk, HIGH);
        mask >>= 1;
        digitalWrite(m_clk, LOW);
    }
    return true;
}

bool MAX7219::send(const quint8 register_number, const quint8 value)
{
//    QElapsedTimer t;
//    t.start();

    digitalWrite(m_ce, LOW);
    quint16 data = (register_number << 8) | value;
    bool send_result = send_16_bits(data);
    digitalWrite(m_ce, HIGH);

    // qDebug() << "LED send took " << (t.nsecsElapsed() / 1.0e6) << "ms";
    return send_result;
}
