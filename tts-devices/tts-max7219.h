//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
// #include <QDebug>

// wiringPi
#include <wiringPi.h>


class MAX7219 : public QObject
{
    Q_OBJECT
public:
    explicit MAX7219(const quint8 ce, const quint8 mosi, const quint8 clk, const quint8 digits, QObject *parent = nullptr);

    bool set_digit(const quint8 digit, const quint8 segments);
    bool set_scan_limit(const quint8 digits);
    bool set_decode_mode(const quint8 mode);
    bool set_display_test(const quint8 test);
    bool set_intensity(const quint8 level);
    bool normal();
    bool shutdown();

signals:

public slots:

private:
    bool send_16_bits(const quint16 value);
    bool send(const quint8 register_number, const quint8 value);

    // MAX7129 Register addresses
    static const quint8 NO_OP = 0x00;
    static const quint8 DIGIT0 = 0x01;
    static const quint8 DIGIT1 = 0x02;
    static const quint8 DIGIT2 = 0x03;
    static const quint8 DIGIT3 = 0x04;
    static const quint8 DIGIT4 = 0x05;
    static const quint8 DIGIT5 = 0x06;
    static const quint8 DIGIT6 = 0x07;
    static const quint8 DIGIT7 = 0x08;
    static const quint8 DECODE_MODE = 0x09;
    static const quint8 INTENSITY = 0x0A;
    static const quint8 SCAN_LIMIT = 0x0B;
    static const quint8 SHUTDOWN = 0x0C;
    static const quint8 DISPLAY_TEST = 0x0F;

    // MAX7219 Register values
    static const quint8 MODE_NONE = 0x00;
    static const quint8 MODE_B = 0xFF;

    quint8 m_ce;
    quint8 m_mosi;
    quint8 m_clk;
};
