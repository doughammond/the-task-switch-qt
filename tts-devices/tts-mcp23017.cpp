//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
// #include <QDebug>
#include <QElapsedTimer>

// tts-devices
#include "tts-mcp23017.h"


InterruptHandler::InterruptHandler(QObject *parent) : QObject(parent)
{

}

InterruptHandler::~InterruptHandler()
{
    stop();
}

void InterruptHandler::start(const quint8 sense_pin)
{
    auto *worker = new InterruptWorker();
    worker->moveToThread(&m_thread);
    connect(
        &m_thread, &QThread::finished,
        worker, &QObject::deleteLater
    );
    connect(
        this, &InterruptHandler::begin,
        worker, &InterruptWorker::watchPin
    );
    connect(
        worker, &InterruptWorker::interrupt,
        this, &InterruptHandler::interrupt
    );
    m_thread.start();
    emit begin(sense_pin);
}

void InterruptHandler::stop()
{
    m_thread.quit();
    m_thread.wait();
}



static quint8 MCP23017_OFFSET = 100;


MCP23017::MCP23017(const quint8 address, QObject *parent) : QObject(parent)
{
    // TODO make thread-safe
    m_offset = MCP23017_OFFSET;
    MCP23017_OFFSET += 16;

    mcp23017Setup(m_offset, address);
    m_fd = wiringPiI2CSetup(address);
}

void MCP23017::set_pin_modes(const quint16 mask)
{
    for (quint8 pin = 0; pin < 16; ++pin) {
        bool val = (mask & (1 << pin)) >> pin;
        if (val) {
            pin_mode(pin, val ? INPUT : OUTPUT);
        }
    }
}

void MCP23017::set_pull_off(const quint16 mask)
{
    for (quint8 pin = 0; pin < 16; ++pin) {
        bool val = (mask & (1 << pin)) >> pin;
        if (val) {
            pull_up_down_control(pin, PUD_OFF);
        }
    }
}

void MCP23017::set_pull_down(const quint16 mask)
{

    for (quint8 pin = 0; pin < 16; ++pin) {
        bool val = (mask & (1 << pin)) >> pin;
        if (val) {
            pull_up_down_control(pin, PUD_DOWN);
        }
    }
}

void MCP23017::set_pull_up(const quint16 mask)
{
    for (quint8 pin = 0; pin < 16; ++pin) {
        bool val = (mask & (1 << pin)) >> pin;
        if (val) {
            pull_up_down_control(pin, PUD_UP);
        }
    }
}


void MCP23017::enable_interrupt(const quint16 mask, const quint16 /*defval*/, const quint8  /*edge_type*/, const quint8 sense_pin)
{
    // we're going to use just one GPIO interrupt input pin
    // for both sets of 8 pins. So, we can set the GPINTEN
    // on both sets at once, and configure the device to
    // mirror interrupts on INTA and INTB

    pinMode(sense_pin, INPUT);
    pullUpDnControl(sense_pin, PUD_DOWN);

    // TODO DEFVAL and INTCON combinations can be used
    // to implement edge_type ?
    //
    // Configuration for the sending side; MCP23017:
    //
    // EDGE_RISING
    // => DEVVAL = 0, INTCON = 1
    // ; default LOW, compare pin with default
    //
    // EDGE_FALLING
    // => DEFVAL = 1, INTCON = 1
    // ; default HIGH, compare pin with default
    //
    // EDGE_BOTH
    // => DEFVAL = 0, INTCON = 0
    // ; default doesn't matter; compare with last state
    //
    //
    // Configuration for the receiving side; RPi:
    // we need to call out to `gpio` for edge_type ?

    quint8 mask_hi = mask >> 8;
    quint8 mask_lo = mask & 0xFF;
    // quint8 defval_hi = defval >> 8;
    // quint8 defval_lo = defval & 0xFF;

    // first configure the IO globally;
    // - BANK   = 0 (registers in same bank, sequential addresses)
    // - MIRROR = 1 (enabled)
    // - SEQOP  = 0 (enabled)
    // - DISSLW = 0 (enabled)
    // - HAEN   = 1 (enabled; fixed enabled on MCP23017)
    // - ODR    = 0 (INT is active driver)
    // - INTPOL = 1 (active high)
    // - X      = 0
    wiringPiI2CWriteReg8(m_fd, IOCON, 0b01001010); delay(50);

    // we need to set the default values to compare the inputs with
    wiringPiI2CWriteReg8(m_fd, DEFVALA, 0); delay(50);
    wiringPiI2CWriteReg8(m_fd, DEFVALB, 0); delay(50);

    // ensure the inputs are compared with last state
    wiringPiI2CWriteReg8(m_fd, INTCONA, 0x0); delay(50);
    wiringPiI2CWriteReg8(m_fd, INTCONB, 0x0); delay(50);

    // then enable the inputs for interrupt
    wiringPiI2CWriteReg8(m_fd, GPINTENA, mask_lo); delay(50);
    wiringPiI2CWriteReg8(m_fd, GPINTENB, mask_hi); delay(50);

    connect(
        &m_inthander, &InterruptHandler::interrupt,
        this, &MCP23017::read_intcap
    );
    m_inthander.start(sense_pin);
}

void MCP23017::disable_interrupts()
{
    m_inthander.stop();
}

bool MCP23017::digital_read(const quint8 pin)
{
    return digitalRead(pin + m_offset);
}

void MCP23017::digital_write(const quint8 pin, const bool val)
{
    digitalWrite(pin + m_offset, val ? HIGH : LOW);
}

quint16 MCP23017::read_all()
{
    return wiringPiI2CReadReg16(m_fd, GPIOA);
}

void MCP23017::write_all(const quint16 data)
{
    wiringPiI2CWriteReg16(m_fd, GPIOA, data);
}

void MCP23017::pin_mode(const quint8 pin, const quint8 mode)
{
    pinMode(pin + m_offset, mode);
}

void MCP23017::pull_up_down_control(const quint8 pin, const quint8 pud)
{
    pullUpDnControl(pin + m_offset, pud);
}

void MCP23017::read_intcap()
{
//    QElapsedTimer t;
//    t.start();

    // read intf - mask of pin(s) which caused interrupt
    const quint16 intf = wiringPiI2CReadReg16(m_fd, INTFA);
    // read intcap - state of IO when interrupt occurred
    // reading this one clears the interrupt condition
    const quint16 intcap = wiringPiI2CReadReg16(m_fd, INTCAPA);

    // qDebug() << "Read interrupt took " << (t.nsecsElapsed() / 1.0e6) << "ms ; intf " << bin << intf << ", intcap " << intcap;
    emit interrupt(intf, intcap);
}
