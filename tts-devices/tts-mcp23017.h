//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QThread>
// #include <QDebug>

// wiringPi
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <mcp23017.h>


class InterruptWorker : public QObject
{
    Q_OBJECT

public slots:
    void watchPin(const quint8 pin) {
        int state = 0;
        int iter = 0;
        // TODO - need a stop condition ?
        while (true) {
            int i = digitalRead(pin);
            if (i == HIGH && state == 0) {
                state = 1;
                // qInfo() << "watch pin got signal: ";
                emit interrupt();
                // qInfo() << "emit interrupt finished";
            } else if (i == LOW) {
                state = 0;
            }
            delay(1);
            if (++iter == 1000) {
                // qInfo() << "interrupt poll alive : " << i << " / " << state;
                iter = 0;
                if (i == 1 && state == 1) {
                    // qInfo() << "reset int detect state";
                    state = 0;
                }
            }
        }
    }

signals:
    void interrupt();
};

class InterruptHandler : public QObject
{
    Q_OBJECT

public:
    InterruptHandler(QObject *parent = nullptr);
    ~InterruptHandler();

    void start(const quint8 sense_pin);
    void stop();

signals:
    void begin(const quint8 pin);
    void interrupt();

private:
    QThread m_thread;
};


class MCP23017 : public QObject
{
    Q_OBJECT
public:
    explicit MCP23017(const quint8 address, QObject *parent = nullptr);

    void set_pin_modes(const quint16 mask);
    void set_pull_off(const quint16 mask);
    void set_pull_down(const quint16 mask);
    void set_pull_up(const quint16 mask);
    void enable_interrupt(const quint16 mask, const quint16 defval, const quint8 edge_type, const quint8 sense_pin);
    void disable_interrupts();

    bool digital_read(const quint8 pin);
    void digital_write(const quint8 pin, const bool val);
    quint16 read_all();
    void write_all(const quint16 data);

    void pin_mode(const quint8 pin, const quint8 mode);
    void pull_up_down_control(const quint8 pin, const quint8 pud);

    quint8 map_pin_number(const quint8 pin) {
        return pin + m_offset;
    }

signals:
    void interrupt(const quint16 trigger, const quint16 state);

public slots:
    void read_intcap();

private:

    // IOCON Bank 0 constants, which is what
    // wiringpi MCP23017 driver seems to use
    static const quint8 GPIOA = 0x12;
    static const quint8 GPINTENA = 0x04;
    static const quint8 GPINTENB = 0x05;
    static const quint8 DEFVALA = 0x06;
    static const quint8 DEFVALB = 0x07;
    static const quint8 INTCONA = 0x08;
    static const quint8 INTCONB = 0x09;
    static const quint8 IOCON = 0x0A;
    static const quint8 INTFA = 0x0E;
    static const quint8 INTFB = 0x0F;
    static const quint8 INTCAPA = 0x10;
    static const quint8 INTCAPB = 0x11;

    quint8 m_offset;
    quint8 m_fd;
    InterruptHandler m_inthander;
};
