//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
// #include <QDebug>
#include <QElapsedTimer>
#include <utility>

// tts-devices
#include "tts-rgblcd.h"

RGBLCD::RGBLCD(const quint8 address,
    RGBLCD_PinMap  pm,
    const quint8 data_width,
    const quint8 rows,
    const quint8 cols,
    QObject *parent
)
    : QObject(parent),
      m_io(new MCP23017(address)),
      m_pm(std::move(pm)),
      m_rows(rows),
      m_cols(cols),
      m_currenttext("")
{
    // LED output setup
    m_io->pin_mode(m_pm.get("r"), OUTPUT);
    m_io->pin_mode(m_pm.get("g"), OUTPUT);
    m_io->pin_mode(m_pm.get("b"), OUTPUT);
    m_io->pull_up_down_control(m_pm.get("r"), PUD_UP);
    m_io->pull_up_down_control(m_pm.get("g"), PUD_UP);
    m_io->pull_up_down_control(m_pm.get("b"), PUD_UP);


    m_lcd = lcdInit(
        m_rows, m_cols, data_width,
        m_io->map_pin_number(m_pm.get("rs")), m_io->map_pin_number(m_pm.get("en")),
        m_io->map_pin_number(m_pm.get("b0")), m_io->map_pin_number(m_pm.get("b1")),
        m_io->map_pin_number(m_pm.get("b2")), m_io->map_pin_number(m_pm.get("b3")),
        m_io->map_pin_number(m_pm.get("b4")), m_io->map_pin_number(m_pm.get("b5")),
        m_io->map_pin_number(m_pm.get("b6")), m_io->map_pin_number(m_pm.get("b7"))
    );
}

void RGBLCD::set_backlight(const bool r, const bool g, const bool b)
{
    m_io->digital_write(m_pm.get("r"), !r);
    m_io->digital_write(m_pm.get("g"), !g);
    m_io->digital_write(m_pm.get("b"), !b);
}

void RGBLCD::set_display(const quint8 display)
{
    lcdDisplay(m_lcd, display);
}

void RGBLCD::set_cursor(const quint8 cursor)
{
    lcdCursor(m_lcd, cursor);
}

void RGBLCD::set_cursor_blink(const quint8 blink)
{
    lcdCursorBlink(m_lcd, blink);
}

void RGBLCD::set_position(const quint8 row, const quint8 col)
{
    lcdPosition(m_lcd, col, row);
}

void RGBLCD::cursor_home()
{
    lcdHome(m_lcd);
}

void RGBLCD::clear_display()
{
    lcdClear(m_lcd);
}

void RGBLCD::write_text(const QString& text)
{
    // quit early if text is not different
    if (text == m_currenttext) {
        // qDebug() << "LCD write text did not change";
        return;
    }

//    QElapsedTimer t;
//    t.start();

    m_currenttext = text;

    QStringList lines = text.split('\n');

    // format each line and write
    QStringList::iterator li = lines.begin();
    quint8 row = 0;
    while (li != lines.end()) {
        if (row > m_rows) {
            continue;
        }
        lcdPosition(m_lcd, 0, row);
        lcdPrintf(m_lcd, "%-16s", lines[row].toStdString().c_str());
        ++li; ++row;
    }

    // qDebug() << "LCD write " << text << " text took " << (t.nsecsElapsed() / 1.0e6) << "ms";
}





RGBLCD_PinMap::RGBLCD_PinMap(const quint8 rs, const quint8 rw, const quint8 en,
    const quint8 b0, const quint8 b1, const quint8 b2, const quint8 b3,
    const quint8 b4, const quint8 b5, const quint8 b6, const quint8 b7,
    const quint8 r,  const quint8 g,  const quint8 b)
{
    m_map["rs"] = rs;
    m_map["rw"] = rw;
    m_map["en"] = en;
    m_map["b0"] = b0;
    m_map["b1"] = b1;
    m_map["b2"] = b2;
    m_map["b3"] = b3;
    m_map["b4"] = b4;
    m_map["b5"] = b5;
    m_map["b6"] = b6;
    m_map["b7"] = b7;
    m_map["r"] = r;
    m_map["g"] = g;
    m_map["b"] = b;
}

quint8 RGBLCD_PinMap::get(const QString& pin_name)
{
    return m_map[pin_name];
}
