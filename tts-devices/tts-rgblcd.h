//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>
#include <QMap>

// wiringPi
#include <lcd.h>

// tts-devices
#include "tts-mcp23017.h"


class RGBLCD_PinMap
{
public:
    RGBLCD_PinMap(
        const quint8 rs, const quint8 rw, const quint8 en,
        const quint8 b0, const quint8 b1, const quint8 b2, const quint8 b3,
        const quint8 b4, const quint8 b5, const quint8 b6, const quint8 b7,
        const quint8 r,  const quint8 g,  const quint8 b
    );

    quint8 get(const QString& pin_name);

private:
    QMap<QString, quint8> m_map;
};


class RGBLCD : public QObject
{
    Q_OBJECT
public:
    explicit RGBLCD(
        const quint8 address,
        RGBLCD_PinMap  pm,
        const quint8 data_width = 4,
        const quint8 rows = 2,
        const quint8 cols = 16,
        QObject *parent = nullptr);

    void set_backlight(const bool r, const bool g, const bool b);
    void set_display(const quint8 display);
    void set_cursor(const quint8 cursor);
    void set_cursor_blink(const quint8 blink);
    void set_position(const quint8 row, const quint8 col);
    void cursor_home();
    void clear_display();
    void write_text(const QString& text);

signals:

public slots:

private:
    MCP23017 *m_io;
    RGBLCD_PinMap m_pm;
    quint8 m_rows;
    quint8 m_cols;
    quint8 m_lcd;

    QString m_currenttext;
};
