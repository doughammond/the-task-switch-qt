//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <QDebug>
#include <QApplication>
#include <QSettings>

// tts-core
#include <ttscontroller.h>
#include <models/ttstsrdatabasemodel.h>
#include <validation.h>

// tts-gui
#include "views/ttsguiview.h"

// TODO remove duplication in tts-pi/main


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationName("dev.lon.tts");
    QApplication::setApplicationName("tts");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    TTSValidatorList vl = {
        new TTSRequireTagsValidator(),
        new TTSNotOnlyURGIMPValidator(),
        new TTSSoloENDValidator()
    };
    TTSAndValidator mv(&vl);

    TTSTSRDatabaseModel ttsm(&mv);

    ViewList views;
    TTSGUIView ttsv;
    views.push_back(&ttsv);

    TTSController ttsc(&ttsm, views, &a);

    ttsv.show();
    return QApplication::exec();
}
