#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

QT       += core gui network

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tts-gui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    views/ttsguiview.cpp

HEADERS += \
    views/ttsguiview.h

FORMS += \
    views/ttsview.ui


target.path = /home/pi/host-build
INSTALLS += target


# static linking tts-core
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../tts-core/release/ -ltts-core
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../tts-core/debug/ -ltts-core
else:unix: LIBS += -L$$OUT_PWD/../tts-core/ -ltts-core

INCLUDEPATH += $$PWD/../tts-core
DEPENDPATH += $$PWD/../tts-core

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../tts-core/release/libtts-core.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../tts-core/debug/libtts-core.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../tts-core/release/tts-core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../tts-core/debug/tts-core.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../tts-core/libtts-core.a

# static linking redux
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../redux/release/ -lredux
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../redux/debug/ -lredux
else:unix: LIBS += -L$$OUT_PWD/../redux/ -lredux

INCLUDEPATH += $$PWD/../redux
DEPENDPATH += $$PWD/../redux

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/release/libredux.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/debug/libredux.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/release/redux.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../redux/debug/redux.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../redux/libredux.a
