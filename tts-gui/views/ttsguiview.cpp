//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
// #include <QDebug>

// tts-gui
#include "ttsguiview.h"
#include "ui_ttsview.h"


TTSGUIViewWindow::TTSGUIViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TTSView)
{
    ui->setupUi(this);
}

TTSGUIViewWindow::~TTSGUIViewWindow()
{
    delete ui;
}


TTSGUIView::TTSGUIView() :
    m_window(new TTSGUIViewWindow())
{
    connect(
        m_window->ui->pb_Action, &QPushButton::pressed,
        this, &IView::action_button_press
    );

    connect(
        m_window->ui->pb_1, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_2, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_3, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_4, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_5, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_6, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_7, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_8, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_9, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
    connect(
        m_window->ui->pb_10, &QPushButton::pressed,
        this, &TTSGUIView::dispatch_tag
    );
}

void TTSGUIView::on_update_display(const DisplayList dsp)
{
    m_window->ui->lbl_Screen->setText(dsp.join("\n"));
}

void TTSGUIView::on_action_status(const bool has_task, const bool has_tags)
{
    //       | task            | !task
    // ------+-----------------+-------------------
    //  tags | pending entry   | pending entry
    //       | active task     | idle
    //       | -> bright green | -> bright green
    // ------+-----------------+-------------------
    // !tags | no entry        | no entry
    //       | active task     | idle
    //       | -> dim red      | -> dim off
    // ------+-----------------+-------------------

    QString intensity = "44";

    if (has_tags) {
        intensity = "DD";
    }

    if (has_task && !has_tags) {
        m_window->ui->lbl_Action->setStyleSheet("QLabel { background-color: #" + intensity + "0000; }");
    } else if (has_tags) {
        m_window->ui->lbl_Action->setStyleSheet("QLabel { background-color: #00" + intensity + "00; }");
    } else {
        m_window->ui->lbl_Action->setStyleSheet("QLabel { background-color: #000000; }");
    }
}


void TTSGUIView::on_update_selected_tags(const TagSet tags)
{
    // stash which tags have been turned on
    QSet<int> tag_on_indexes;

    TagSet::const_iterator ti = tags.constBegin();
    while (ti != tags.constEnd()) {
        QString tag = *ti;
        if (m_user_tags.contains(tag))
        {
            const int tag_index = m_user_tags.indexOf(tag);
            tag_on_indexes.insert(tag_index);

            // turn on the selected tag
            QString label_id = QStringLiteral("led_%1").arg(tag_index);
            // qInfo() << "toggle LED index " << tag_index << " label " << label_id;
            auto *tag_label = m_window->findChild<QLabel*>(label_id);
            if (tag_label) {
                // qInfo() << "LED label is valid";
                tag_label->setStyleSheet("QLabel { background-color: #00CC00; }");
            }
        }

        ++ti;
    }

    // turn off the unselected tags
    for (int i = 0; i < 10; ++i) {
        if (!tag_on_indexes.contains(i)) {
            QString label_id = QStringLiteral("led_%1").arg(i);
            // qInfo() << "toggle LED label " << label_id;
            auto *tag_label = m_window->findChild<QLabel*>(label_id);
            if (tag_label) {
                // qInfo() << "LED label is valid";
                tag_label->setStyleSheet("QLabel { background-color: none; }");
            }
        }
    }
}

void TTSGUIView::on_status_change(const QString status)
{
    if (status == "error") {
        m_window->ui->lbl_Screen->setStyleSheet("QLabel { color: #CC0000; }");
    } else if (status == "success") {
        m_window->ui->lbl_Screen->setStyleSheet("QLabel { color: #00CC00; }");
    } else if (status == "syncing") {
        m_window->ui->lbl_Screen->setStyleSheet("QLabel { color: #0000CC; }");
    } else {
        m_window->ui->lbl_Screen->setStyleSheet("QLabel { color: none; }");
    }
}

void TTSGUIView::on_get_user_tags(const TagList tags) {
    m_user_tags.clear();
    m_user_tags.append(tags);


    switch (m_user_tags.size()) {
        case 10:
            m_window->ui->pb_10->setText(m_user_tags[9]);
            [[clang::fallthrough]];
        case 9:
            m_window->ui->pb_9->setText(m_user_tags[8]);
            [[clang::fallthrough]];
        case 8:
            m_window->ui->pb_8->setText(m_user_tags[7]);
            [[clang::fallthrough]];
        case 7:
            m_window->ui->pb_7->setText(m_user_tags[6]);
            [[clang::fallthrough]];
        case 6:
            m_window->ui->pb_6->setText(m_user_tags[5]);
            [[clang::fallthrough]];
        case 5:
            m_window->ui->pb_5->setText(m_user_tags[4]);
            [[clang::fallthrough]];
        case 4:
            m_window->ui->pb_4->setText(m_user_tags[3]);
            [[clang::fallthrough]];
        case 3:
            m_window->ui->pb_3->setText(m_user_tags[2]);
            [[clang::fallthrough]];
        case 2:
            m_window->ui->pb_2->setText(m_user_tags[1]);
            [[clang::fallthrough]];
        case 1:
            m_window->ui->pb_1->setText(m_user_tags[0]);
            [[clang::fallthrough]];
        case 0:
            break;
    }
}

void TTSGUIView::dispatch_tag()
{
    auto *b = dynamic_cast<QPushButton*>(sender());
    QString tag = b->text();
    if (m_user_tags.contains(tag)) {
        //    qInfo() << "Tag button pressed : " << b->text();
        const int tagindex = m_user_tags.indexOf(tag);
        emit tag_button_press(tagindex);
    }
}
