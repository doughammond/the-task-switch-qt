//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

#include <QMainWindow>

#include <interfaces/ttsview.h>

namespace Ui {
class TTSView;
}

class TTSGUIViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TTSGUIViewWindow(QWidget *parent = 0);
    ~TTSGUIViewWindow();

    // This is not normally public, but in this application
    // the contents of this class will be directly controlled
    // by the TTSGUIView below.
    Ui::TTSView *ui;
};



class TTSGUIView : public IView
{
    Q_OBJECT

public:
    TTSGUIView();

    void show() {
        m_window->show();
    }

public slots:
    void on_update_display(const DisplayList dsp);
    void on_action_status(const bool has_task, const bool has_tags);
    void on_update_selected_tags(const TagSet tags);
    void on_status_change(const QString status);
    void on_get_user_tags(const TagList tags);

private:
    void dispatch_tag();

    TTSGUIViewWindow *m_window;
};
