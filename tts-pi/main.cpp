//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <QDebug>
#include <QCoreApplication>
#include <QSettings>

// tts-core
#include <ttscontroller.h>
#include <models/ttstsrdatabasemodel.h>
#include <validation.h>

// tts-pi
#include "views/ttshardwareview.h"

// TODO remove duplication in tts-gui/main

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCoreApplication::setOrganizationName("dev.lon.tts");
    QCoreApplication::setApplicationName("tts");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    wiringPiSetup();

    TTSValidatorList vl = {
        new TTSRequireTagsValidator(),
        new TTSNotOnlyURGIMPValidator(),
        new TTSSoloENDValidator()
    };
    TTSAndValidator mv(&vl);

    TTSTSRDatabaseModel ttsm(&mv);

    ViewList views;
    TTSHardwareView ttsvp;
    views.push_back(&ttsvp);

    TTSController ttsc(&ttsm, views, &a);

    return a.exec();
}
