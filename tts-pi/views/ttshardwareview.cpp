//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

// Qt
#include <QMap>
//#include <QDebug>
#include <QElapsedTimer>

// tts-pi
#include "views/ttshardwareview.h"


// map {tag : (digit, segment)}
typedef QMap<quint8, QPair<quint8, quint8>> TagLedMap;
static const TagLedMap TAG_LEDS = {
    { 6, {0, 1 << 0} },
    { 5, {0, 1 << 1} },
    { 4, {0, 1 << 2} },
    { 3, {0, 1 << 3} },
    { 2, {0, 1 << 4} },
    { 1, {0, 1 << 5} },
    { 0, {0, 1 << 6} },
    { 7, {0, 1 << 7} },

    { 9, {1, 1 << 5} },
    // {1, 1 << 3} is action "red"
    // {1, 1 << 4} is action "green"
    { 8,  {1, 1 << 6} }
};



TTSHardwareView::TTSHardwareView(QObject *parent)
    : IView(parent),
      m_io0(new MCP23017(0x21)),
      m_led0(new MAX7219(10, 12, 14, 8)),
      m_lcd0(new RGBLCD(
        0x20,
        RGBLCD_PinMap(
            // rs, rw, en, b0, b1, b2, b3, b4, b5, b6, b7, r, g, b
            0,     99,  1,  2,  3,  4,  5,  9, 10, 11, 12, 6, 7, 8
        ),
        4
      )),
      m_led0_d1_state(0)
{
    wiringPiSetup();

    // Set up LED driver;
    // D0; S0 - S7 are tag button LEDS
    // D1; S1,  S6 are tag button LEDS
    // D1; S3 is action button "red"
    // D1; S4 is action button "green"
    // We need to stash the D1 state so
    // that tag LED changes do not clobber
    // the action LED states.
    // D0 state is completely tag state
    // driven and does not need to be cached.

    // Reset button LEDs
    m_led0->set_digit(0, 0x00);
    m_led0->set_digit(1, m_led0_d1_state);


    // Set up MCP23017 GPIO #1
    // A0 - A7 are tag buttons
    // B0 - B1 are tag buttons
    // B2 is the action button

    // We need 10 pins as inputs for the tag buttons,
    // and 1 input as the action button.
    // All with interrupt enabled.
    connect(
        m_io0, &MCP23017::interrupt,
        this, &TTSHardwareView::on_io0_interrupt
    );
    //                        BBBBBBBBAAAAAAAA
    //                        7654321076543210
    m_io0->set_pin_modes(   0b0000000000000000); delay(250);
    // m_io0->set_pull_off(    0b1111111111111111); delay(250);
    m_io0->set_pull_up(     0b1111111111111111); delay(250);
    m_io0->enable_interrupt(0b0000011111111111, 0xFFFF, INT_EDGE_RISING, 0);
}

TTSHardwareView::~TTSHardwareView()
{
    // reset hardware
    m_io0->set_pull_off(0b1111111111111111);
    m_lcd0->clear_display();
    m_lcd0->set_backlight(false, false, false);
}

void TTSHardwareView::show()
{
    // init LCD - turn the lights on :)
    m_lcd0->set_backlight(true, true, true);
}

void TTSHardwareView::on_update_display(const DisplayList dsp)
{
    m_lcd0->write_text(dsp.join("\n"));
}

void TTSHardwareView::on_action_status(const bool has_task, const bool has_tags)
{
    // Use the D1 state as a start
    quint8 state = m_led0_d1_state;

    //       | task            | !task
    // ------+-----------------+-------------------
    //  tags | pending entry   | pending entry
    //       | active task     | idle
    //       | -> bright green | -> bright green
    // ------+-----------------+-------------------
    // !tags | no entry        | no entry
    //       | active task     | idle
    //       | -> dim red      | -> dim off
    // ------+-----------------+-------------------

    quint8 intensity = 10;
    if (has_tags) {
        intensity = 15;
    }

    // qInfo() << "on action status : has_task=" << has_task << " has_tags=" << has_tags;
    // qInfo() << "on action status : current state " << bin << state;

    state &= 0xFF - (1 << 3 | 1 << 4);

    if (has_task && !has_tags) {
        // D1 S4 is "red"
        state |= 1 << 3;
    } else if (has_tags) {
        // D1 S5 is "green"
        state |= 1 << 4;
    } // else off

    // qInfo() << "on action status : stored state " << bin << m_led0_d1_state;
    // qInfo() << "on action status : next   state " << bin << state;

    if (state != m_led0_d1_state) {
        m_led0_d1_state = state;
        m_led0->set_digit(1, state);
        m_led0->set_intensity(intensity);
    }
}

void TTSHardwareView::on_update_selected_tags(const TagSet tags)
{
    // Use the D1 state as a start so that we maintain the action LEDs state
    quint8 digit1_state = m_led0_d1_state;
    QList<quint8> digits = {
        0x00,
        digit1_state
    };
    TagLedMap::const_iterator tli = TAG_LEDS.begin();
    while (tli != TAG_LEDS.end()) {
        const quint8 tagindex = tli.key();
        if (tagindex < m_user_tags.size()) {
            const QString tag = m_user_tags[tagindex];
            const QPair<quint8, quint8> ds = tli.value();
            if (tags.contains(tag)) {
                digits[ds.first] |= ds.second;
            } else {
                digits[ds.first] &= 0xFF - ds.second;
            }
        }
        ++tli;
    }

    m_led0_d1_state = digits[1];

    m_led0->set_digit(0, digits[0]);
    m_led0->set_digit(1, digits[1]);
}

void TTSHardwareView::on_status_change(const QString status)
{
    if (status == "error") {
        m_lcd0->set_backlight(true, false, false);
    } else if (status == "success") {
        m_lcd0->set_backlight(false, true, false);
    } else if (status == "syncing" ) {
        m_lcd0->set_backlight(false, false, true);
    } else {
        m_lcd0->set_backlight(true, true, true);
    }
}

void TTSHardwareView::on_get_user_tags(const TagList tags)
{
    // TODO; if the hardware unit ever gets 2nd screen to show the user tags,
    // then update it here
    m_user_tags.clear();
    m_user_tags.append(tags);
//    qInfo() << "Set user tags to" << m_user_tags;
}

void TTSHardwareView::on_io0_interrupt(const quint16 /*trigger*/, const quint16 state)
{
//    QElapsedTimer t;
//    t.start();
//    qInfo() << "handle interrupt for" << bin << (0xFFFF - state);

    for (size_t i = 0; i < 10; ++i) {
        auto mask = 1 << i;
        // push buttons use negative logic
        if (!(state & mask)) {
            emit tag_button_press(i);
        }
    }

    // B2 is the action button
    if (!(state & 0b0000010000000000)) {
        emit action_button_press();
    }

    // qDebug() << "io0 interrupt took " << (t.nsecsElapsed() / 1.0e6) << "ms";
}
