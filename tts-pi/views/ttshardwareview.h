//
// SPDX-License-Identifier: GPL-3.0-only
//
// Copyright (C) 2018 Doug Hammond
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

#pragma once

// Qt
#include <QObject>

// tts-core
#include <interfaces/ttsview.h>

// tts-devices
#include <tts-max7219.h>
#include <tts-mcp23017.h>
#include <tts-rgblcd.h>


class TTSHardwareView : public IView
{
    Q_OBJECT

public:
    explicit TTSHardwareView(QObject *parent = nullptr);
    ~TTSHardwareView();

    void show();

public slots:
    void on_update_display(const DisplayList dsp);
    void on_action_status(const bool has_task, const bool has_tags);
    void on_update_selected_tags(const TagSet tags);
    void on_status_change(const QString status);
    void on_get_user_tags(const TagList tags);

private slots:
    void on_io0_interrupt(const quint16 trigger, const quint16 state);

signals:

private:
    MCP23017 *m_io0;
    MAX7219 *m_led0;
    RGBLCD *m_lcd0;
    quint8 m_led0_d1_state;

    TagList m_user_tags;
};
