#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    redux \
    redux-test \
    tts-core \
    tts-gui

redux-test.depends = redux
tts-core.depends = redux
tts-gui.depends = redux tts-core


onDevice {
    message("USING onDevice CONFIG")
    SUBDIRS += \
        tts-devices \
        tts-pi \
        devices-test
    tts-pi.depends = redux tts-core tts-devices
    devices-test.depends = tts-devices
}
